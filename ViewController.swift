//
//  ViewController.swift
//  Colorful Hello World
//
//  Created by Victor Hernandez on 10/22/14.
//  Copyright (c) 2014 Compubility. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    

    @IBOutlet weak var nameInput: UITextField!
    
    @IBOutlet weak var greetingString: UILabel!
    
    @IBAction func GreetingButtonPressed(sender: AnyObject) {
        if (nameInput != nil){
            greetingString.text = "Hello \(nameInput.text)";
        }
        else {
         greetingString.text = "Hello NEW WORLD!"
        }
        for var i = 0 ; i < greetingString.text?.utf16Count ; i++ {
            var randomColorValues = randomColor()
           greetingString.textColor = UIColor(red: CGFloat(randomColorValues[0]), green: CGFloat(randomColorValues[1]), blue: CGFloat(randomColorValues[2]), alpha: 1)
        }
    }
    
}


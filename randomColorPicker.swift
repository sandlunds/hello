//
//  randomColorPicker.swift
//  Colorful Hello World
//
//  Created by Victor Hernandez on 10/22/14.
//  Copyright (c) 2014 Compubility. All rights reserved.
//

import Foundation

// This will generate and return a random color to be used as font color

// Xcode keeps complaining when trying to pass Array<CGFloat> as the return type so a float was returned and typecasted when used

func randomColor() -> Array<Float>{
    
    var redFloat = Float( (rand() % 256)) / 255.0
    var greenFloat = Float( (rand() % 256)) / 255.0
    var blueFloat = Float( (rand() % 256)) / 255.0
    var alphaFloat  = Float( (rand() % 256)) / 255.0
    
    return [redFloat, greenFloat,blueFloat,alphaFloat];
    
}